var imatecQuestionsApp = angular.module("imatecQuestionsApp", []);

imatecQuestionsApp.controller("imatecQuestionsCtrl", ["$scope", "$window", function($scope, $window) {
    // var file = new File(["../js/imatec1.json"], "imatec1");
    // var reader = new $window.FileReader();
    // reader.onloadend = function(evt) {
    //     console.log("read success");
    //     console.log(evt.target.result);
    // };
    // reader.readAsDataURL(file);
    // console.log(reader)

    $scope.toggle = function(level) {
        switch (level) {
            case 1:
                for (var i = 0; i < $scope.questions1.items.length; i++) {
                    if ($scope.questions1.items[i].done == false) {
                        $scope.questions1.items[i].done = true;
                    } else {
                        $scope.questions1.items[i].done = false;
                    }
                }
                break;

            case 2:
                for (var i = 0; i < $scope.questions2.items.length; i++) {
                    if ($scope.questions2.items[i].done == false) {
                        $scope.questions2.items[i].done = true;
                    } else {
                        $scope.questions2.items[i].done = false;
                    }
                }
                break;

            case 3:
                for (var i = 0; i < $scope.questions3.items.length; i++) {
                    if ($scope.questions3.items[i].done == false) {
                        $scope.questions3.items[i].done = true;
                    } else {
                        $scope.questions3.items[i].done = false;
                    }
                }
                break;

            case 4:
                for (var i = 0; i < $scope.questions4.items.length; i++) {
                    if ($scope.questions4.items[i].done == false) {
                        $scope.questions4.items[i].done = true;
                    } else {
                        $scope.questions4.items[i].done = false;
                    }
                }
                break;

            case 5:
                for (var i = 0; i < $scope.questions5.items.length; i++) {
                    if ($scope.questions5.items[i].done == false) {
                        $scope.questions5.items[i].done = true;
                    } else {
                        $scope.questions5.items[i].done = false;
                    }
                }
                break;
        }
    }

    $scope.incompleteCount1 = function(node, subsystem, assembly, component) {
        $scope.questions1 = imatec[0];
        var count = 0;
        angular.forEach($scope.questions1.items, function(question) {
            if (question.done) {
                count++
            }
        });
        if (count == imatec[0].items.length) {
            objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 1;
        } else {
            objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 0;
        }
        console.log(objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec);
        return count;
    }

    $scope.incompleteCount2 = function() {
        if (objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec == 1) {
            $scope.questions2 = imatec[1];
            var count = 0;
            angular.forEach($scope.questions2.items, function(question) {
                if (question.done) {
                    count++
                }
            });
            if (count == imatec[1].items.length) {
                objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 2;
            } else {
                objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 1;
            }
            console.log(objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec);
            return count;
        }
    }


    $scope.incompleteCount3 = function() {
        if (objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec == 2) {
            $scope.questions3 = imatec[2];
            var count = 0;
            angular.forEach($scope.questions3.items, function(question) {
                if (question.done) {
                    count++
                }
            });
            if (count == imatec[2].items.length) {
                objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 3;
            } else {
                objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 2;
            }
            console.log(objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec);
            return count;
        }
    }

    $scope.incompleteCount4 = function() {
        $scope.questions4 = imatec[3];
        var count = 0;
        angular.forEach($scope.questions4.items, function(question) {
            if (question.done) {
                count++
            }
        });
        if (count == imatec[3].items.length) {
            objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 3;
        } else {
            objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 2;
        }
        console.log(objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec);
        return count;
    }

    $scope.incompleteCount5 = function() {
        $scope.questions5 = imatec[4];
        var count = 0;
        angular.forEach($scope.questions5.items, function(question) {
            if (question.done) {
                count++
            }
        });
        if (count == imatec[4].items.length) {
            objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 3;
        } else {
            objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec = 2;
        }
        console.log(objProduto["0"].nodes["0"].nodes["0"].nodes["0"].imatec);
        return count;
    }
}]);

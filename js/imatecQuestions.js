var produto='['+
  '{'+
    '"id": 1,'+
    '"title": "Produto 1",'+
    '"type": "product",'+
    '"imatec": 0,'+
    '"nodes": ['+
      '{'+
        '"id": 10,'+
        '"title": "Subsistema.1",'+
        '"type": "subsystem",'+
        '"imatec": 0,'+
        '"nodes": ['+
          '{'+
            '"id": 100,'+
            '"title": "Montagem.1",'+
            '"type": "assembly",'+
            '"imatec": 0,'+
            '"nodes": ['+
              '{'+
                '"id": 1000,'+
                '"title": "Componente.1",'+
                '"type": "component",'+
                '"imatec": 0'+
              '}'+
            ']'+
          '}'+
        ']'+
      '}'+
    ']'+
  '}'+
']';


var objProduto = JSON.parse(produto);

//Componente
var nodeToEvaluate='0,0,0,0';
//Montagem
//nodeToEvaluate=objProduto["0"].nodes["0"].nodes["0"].imatec;
//Subsistema
//nodeToEvaluate=objProduto["0"].nodes["0"].imatec;

var type="software";
var imatec=[];

switch (type){
    case "hardware":
        imatec[0] = {items: [
            {action: "A hipótese da pesquisa foi formulada?",done: false},
            {action: "Os princípios científicos básicos foram observados?",done: false},
            {action: "As leis físicas e as hipóteses que sustentam às observações foram definidos?",done: false},
            {action: "As leis físicas e as hipóteses que sustentam às observações foram verificadas?",done: false},
            {action: "Os elementos básicos da tecnologia foram identificados?",done: false},
            {action: "Hipóteses subjacentes geraram conhecimento científico?",done: false},
            {action: "Os princípios básicos foram confirmados por publicação em periódico com revisão por pares?",done: false}]
        };
        break;
    case "software":
        imatec[0] = {items: [
            {action: "A hipótese da pesquisa foi formulada?",done: false},
            {action: "Os princípios científicos básicos foram observados?",done: false},
            {action: "Funções algorítmicas e as hipóteses que sustentam às observações foram definidos?",done: false},
            {action: "Funções algorítmicas e as hipóteses que sustentam às observações foram verificadas?",done: false},
            {action: "Formulações matemáticas básicas foram identificadas?",done: false},
            {action: "Hipóteses subjacentes geraram conhecimento científico?",done: false},
            {action: "Os princípios básicos foram confirmados por publicação em periódico com revisão por pares?",done: false}]
        };
        break;
}

switch (type){
    case "hardware":
        imatec[1] = {items: [
            {action: "Um conceito foi formulado?",done: false},
            {action: "Foram identificados os princípios científicos básicos que sustentam o conceito?",done: false},
            {action: "Estudos analíticos preliminares confirmam o conceito básico?",done: false},
            {action: "A aplicação foi identificada?",done: false},
            {action: "A solução do projeto preliminar foi identificada?",done: false},
            {action: "Estudos de sistemas preliminares mostram ser viável?",done: false},
            {action: "Foram feitas as previsões de desempenho preliminares?",done: false},
            {action: "Modelagem e Simulação (M&S) foram usadas para refinar ainda mais as previsões de desempenho e para confirmar os benefícios?",done: false},
            {action: "Os benefícios foram formulados?",done: false},
            {action: "A abordagem de pesquisa e de desenvolvimento foi formulada?",done: false},
            {action: "A definição preliminar de testes de laboratório e de ambientes foi estabelecida?",done: false},
            {action: "A viabilidade e benefícios do conceito/aplicação de foram relatados em revistas/ anais de conferências/relatórios técnicos científicos?",done: false}]
        };
        break;
    case "software":
        imatec[1] = {items: [
            {action: "Um conceito foi formulado?",done: false},
            {action: "Os princípios científicos básicos que sustentam o conceito foram identificados?",done: false},
            {action: "Propriedades básicas de algoritmos, representações e conceitos foram definidos?",done: false},
            {action: "Os estudos analíticos preliminares confirmam o conceito básico?",done: false},
            {action: "Aplicação foi identificada?",done: false},
            {action: "Solução do projeto preliminar foi identificado?",done: false},
            {action: "Estudos de sistemas preliminares mostram que a aplicação é viável?",done: false},
            {action: "Foram feitas as previsões de desempenho preliminares?",done: false},
            {action: "Os princípios básicos foram codificados?",done: false},
            {action: "Modelagem e Simulação foram usadas para refinar ainda mais as previsões de desempenho e para confirmar os benefícios?",done: false},
            {action: "Os benefícios foram formuladas?",done: false},
            {action: "A abordagem de pesquisa e de desenvolvimento foi formulada?",done: false},
            {action: "As definições preliminares de testes de laboratório e de ambientes foram estabelecidas?",done: false},
            {action: "As experiências foram realizadas com dados de síntese?",done: false},
            {action: "A viabilidade e benefícios do conceito/aplicação foram relatados em revistas/ anais de conferências/relatórios técnicos científicos?",done: false}]
        };
        break;
}

switch (type){
    case "hardware":
        imatec[2] = {items: [
            {action: "Foram identificadas as funções críticas/componentes do conceito/aplicação?",done: false},
            {action: "Foram feitas previsões analíticas do subsistema ou componentes?",done: false},
            {action: "O desempenho do subsistema ou componente foi avaliado por Modelagem e Simulação?",done: false},
            {action: "Foram estabelecidas as métricas do desempenho dos principais parâmetros preliminares?",done: false},
            {action: "Foram estabelecidos os testes de laboratório e de ambientes?",done: false},
            {action: "Equipamento de apoio ao teste de laboratório e instalações foram concluídos para testes de componentes/prova-de-conceito?",done: false},
            {action: "Foi concluída a aquisição/fabricação do componente?",done: false},
            {action: "Foi concluído o testes dos componentes?",done: false},
            {action: "Foram concluídas as análise dos resultados dos testes estabelecendo as principais métricas de desempenho para componentes/subsistemas?",done: false},
            {action: "Foi realizada a verificação analítica de funções críticas de prova-de-conceito?",done: false},
            {action: "Foram documentadas as provas-de-conceito analíticas e experimentais?",done: false}]
        };
        break;
    case "software":
        imatec[2] = {items: [
            {action: "Funções críticas/componentes do conceito/ aplicação foram identificados?",done: false},
            {action: "Foram feitas previsões analíticas do subsistema ou componentes?",done: false},
            {action: "O desempenho do subsistema ou componente foi avaliado por Modelagem e Simulação?",done: false},
            {action: "Foram estabelecidas as métricas do desempenho dos principais parâmetros preliminares?",done: false},
            {action: "Foram estabelecidos os testes de laboratório e de ambientes?",done: false},
            {action: "Equipamento de teste de laboratório e ambiente de computação foram concluídas para testes de componentes/prova-de-conceito?",done: false},
            {action: "Foi concluída a aquisição/codificação do componente?",done: false},
            {action: "V & V do componente foram concluídas? ",done: false},
            {action: "Análise dos resultados dos testes foram concluídos estabelecendo as principais métricas de desempenho para componentes/subsistemas?",done: false},
            {action: "Foi realizada a verificação analítica de funções críticas de prova-de-conceito?",done: false},
            {action: "Foram documentadas as provas-de-conceito analíticas e experimentais?",done: false}]
        };
        break;
    case "manufacturing":
        imatec[2] = {items: [
            {action: "Existe o projeto preliminar de componentes/subsistema/sistemas a ser fabricado?",done: false},
            {action: "Os requisitos de fabricação básicos foram identificados?",done: false},
            {action: "Os conceitos de fabricação atuais foram avaliados?",done: false},
            {action: "Foram realizadas modificações necessárias para os conceitos de fabricação existente?",done: false},
            {action: "Foram necessários novos conceitos de fabricação?",done: false},
            {action: "Foram identificados os requisitos para materiais novos, componentes, competências e instalações? ",done: false},
            {action: "Foi identificado o fluxo do processo preliminar?",done: false},
            {action: "Foram identificados os conceitos de fabricação obrigatórios?",done: false}]
        };
        break;
}

switch (type){
    case "hardware":
        imatec[3] = {items: [
            {action: "Conceito/aplicação foram traduzidos em detalhe para o nível do projeto da breadboard do sistema/subsistema/componente?",done: false},
            {action: "A definição preliminar de ambiente operacional foi concluída?",done: false},
            {action: "Os testes de laboratório e de ambientes foram definidos para o teste de breadboard?",done: false},
            {action: "As Previsões do pré-teste de performance da breadboard em um ambiente de laboratório foram avaliadas por Modelagem e Simulação?",done: false},
            {action: "As métricas de desempenho do parâmetro principal foram estabelecidas para os testes de laboratoriais da breadboard?",done: false},
            {action: "Equipamento de apoio para o teste de laboratório e as instalações foram concluídas para o teste da breadboard?",done: false},
            {action: "A fabricação da breadboard de nível de Sistema/subsistema/componente foi concluída? ",done: false},
            {action: "Os testes de breadboard foram completados?",done: false},
            {action: "As análises dos resultados dos testes foram concluídas verificando o desempenho de acordo com as previsões?",done: false},
            {action: "Foram definidos os requisitos dos sistemas preliminares para a aplicação do usuário final?",done: false},
            {action: "Os ambientes de teste crítico e previsões de desempenho foram definidos em relação à definições preliminares do ambiente operacional?",done: false},
            {action: "O ambiente de teste relevante foi definido?",done: false},
            {action: "Foram documentados os resultados de desempenho da Breadboard verificando as previsões analíticas e definições do ambiente operacional relevantes?",done: false}]
        };
        break;
    case "software":
        imatec[3] = {items: [
            {action: "Conceito/aplicação foram traduzidos em detalhe para o nível do projeto da arquitetura de software do sistema/subsistema/componente?",done: false},
            {action: "A definição preliminar de ambiente operacional foi concluída?",done: false},
            {action: "Os testes de laboratório e de ambientes foram definidos para o teste de componente integrado?",done: false},
            {action: "As previsões do pré-teste da performance de componente integrado em ambiente de laboratório foram avaliadas por Modelagem e Simulação?",done: false},
            {action: "As métricas de desempenho do parâmetro principal foram estabelecidas para os testes de laboratoriais de componente integrado?",done: false},
            {action: "Equipamento de apoio para o teste de laboratório e ambiente computacional foram concluídas para o teste de componente integrado?",done: false},
            {action: "A codificação de nível de Sistema/subsistema/componente foi concluída?",done: false},
            {action: "Testes de componente integrado foi concluído?",done: false},
            {action: "As análises dos resultados dos testes foram concluídas verificando o desempenho de acordo com as previsões?",done: false},
            {action: "Foram definidos os requisitos dos sistemas preliminares para a aplicação do usuário final?",done: false},
            {action: "Os ambientes de teste crítico e previsões de desempenho foram definidos em relação à definições preliminares do ambiente operacional?",done: false},
            {action: "O ambiente de teste relevante foi definido?",done: false},
            {action: "Foram documentados os resultados de desempenho de componente integrado verificando previsões analíticas e definição de ambiente operacional relevantes?",done: false},
            {action: "Testes de componentes integrados foram concluídos para o código reutilizado?",done: false},
            {action: "Foram documentados os resultados de desempenho de componente integrado verificando previsões analíticas e definição de ambiente operacional relevantes?",done: false}]
        };
        break;
    case "manufacturing":
        imatec[3] = {items: [
            {action: "Os requisitos de fabricação foram finalizados (incluindo testes)?",done: false},
            {action: "Foram identificados os requisitos de maquinário/ferramenta?",done: false},
            {action: "Foi identificado o treinamento/certificação para todas as habilidades necessárias (principalmente as novas competências)?",done: false},
            {action: "Foram identificados os materiais necessários? ",done: false},
            {action: "Iniciou-se a avaliação de produtibilidade?",done: false},
            {action: "Foi implementado no breadboard as modificações de maquinário/ferramentas?",done: false},
            {action: "Foram identificados os principais processos de fabricação?",done: false},
            {action: "As novas máquinas/ferramentas foram implementados no breadboard?",done: false},
            {action: "Foram identificados requisitos de metrologia?",done: false},
            {action: "As principais componentes de metrologia foram implementados no breadboard?",done: false},
            {action: "Foram identificados os principais requisitos de ferramentas analíticas?",done: false},
            {action: "As principais ferramentas analíticas foram implantadas no breadboard?",done: false},
            {action: "Os principais processos de fabricação foram validados em laboratório?",done: false},
            {action: "Foram identificadas estratégias de mitigação para resolver as deficiências de fabricação/produtibilidade?",done: false},
            {action: "Todos os processos de fabricação foram identificados?",done: false}]
        };
        break;
}

switch (type){
    case "hardware":
        imatec[4] = {items: [
            {action: "Conceito/aplicação foram traduzidos em detalhe para o nível do projeto da breadboard do sistema/subsistema/componente?",done: false},
            {action: "A definição preliminar de ambiente operacional foi concluída?",done: false},
            {action: "Os testes de laboratório e de ambientes foram definidos para o teste de breadboard?",done: false},
            {action: "As Previsões do pré-teste de performance da breadboard em um ambiente de laboratório foram avaliadas por Modelagem e Simulação?",done: false},
            {action: "As métricas de desempenho do parâmetro principal foram estabelecidas para os testes de laboratoriais da breadboard?",done: false},
            {action: "Equipamento de apoio para o teste de laboratório e as instalações foram concluídas para o teste da breadboard?",done: false},
            {action: "A fabricação da breadboard de nível de Sistema/subsistema/componente foi concluída? ",done: false},
            {action: "Os testes de breadboard foram completados?",done: false},
            {action: "As análises dos resultados dos testes foram concluídas verificando o desempenho de acordo com as previsões?",done: false},
            {action: "Foram definidos os requisitos dos sistemas preliminares para a aplicação do usuário final?",done: false},
            {action: "Os ambientes de teste crítico e previsões de desempenho foram definidos em relação à definições preliminares do ambiente operacional?",done: false},
            {action: "O ambiente de teste relevante foi definido?",done: false},
            {action: "Foram documentados os resultados de desempenho da Breadboard verificando as previsões analíticas e definições do ambiente operacional relevantes?",done: false}]
        };
        break;
    case "software":
        imatec[4] = {items: [
            {action: "Conceito/aplicação foram traduzidos em detalhe para o nível do projeto da arquitetura de software do sistema/subsistema/componente?",done: false},
            {action: "A definição preliminar de ambiente operacional foi concluída?",done: false},
            {action: "Os testes de laboratório e de ambientes foram definidos para o teste de componente integrado?",done: false},
            {action: "As previsões do pré-teste da performance de componente integrado em ambiente de laboratório foram avaliadas por Modelagem e Simulação?",done: false},
            {action: "As métricas de desempenho do parâmetro principal foram estabelecidas para os testes de laboratoriais de componente integrado?",done: false},
            {action: "Equipamento de apoio para o teste de laboratório e ambiente computacional foram concluídas para o teste de componente integrado?",done: false},
            {action: "A codificação de nível de Sistema/subsistema/componente foi concluída?",done: false},
            {action: "Testes de componente integrado foi concluído?",done: false},
            {action: "As análises dos resultados dos testes foram concluídas verificando o desempenho de acordo com as previsões?",done: false},
            {action: "Foram definidos os requisitos dos sistemas preliminares para a aplicação do usuário final?",done: false},
            {action: "Os ambientes de teste crítico e previsões de desempenho foram definidos em relação à definições preliminares do ambiente operacional?",done: false},
            {action: "O ambiente de teste relevante foi definido?",done: false},
            {action: "Foram documentados os resultados de desempenho de componente integrado verificando previsões analíticas e definição de ambiente operacional relevantes?",done: false},
            {action: "Testes de componentes integrados foram concluídos para o código reutilizado?",done: false},
            {action: "Foram documentados os resultados de desempenho de componente integrado verificando previsões analíticas e definição de ambiente operacional relevantes?",done: false}]
        };
        break;
    case "manufacturing":
        imatec[4] = {items: [
            {action: "Os requisitos de fabricação foram finalizados (incluindo testes)?",done: false},
            {action: "Foram identificados os requisitos de maquinário/ferramenta?",done: false},
            {action: "Foi identificado o treinamento/certificação para todas as habilidades necessárias (principalmente as novas competências)?",done: false},
            {action: "Foram identificados os materiais necessários? ",done: false},
            {action: "Iniciou-se a avaliação de produtibilidade?",done: false},
            {action: "Foi implementado no breadboard as modificações de maquinário/ferramentas?",done: false},
            {action: "Foram identificados os principais processos de fabricação?",done: false},
            {action: "As novas máquinas/ferramentas foram implementados no breadboard?",done: false},
            {action: "Foram identificados requisitos de metrologia?",done: false},
            {action: "As principais componentes de metrologia foram implementados no breadboard?",done: false},
            {action: "Foram identificados os principais requisitos de ferramentas analíticas?",done: false},
            {action: "As principais ferramentas analíticas foram implantadas no breadboard?",done: false},
            {action: "Os principais processos de fabricação foram validados em laboratório?",done: false},
            {action: "Foram identificadas estratégias de mitigação para resolver as deficiências de fabricação/produtibilidade?",done: false},
            {action: "Todos os processos de fabricação foram identificados?",done: false}]
        };
        break;
}

    // case " ":
    //     imatec[ ] = {items: [
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false},
    //         {action: "",done: false}]
    //     };
    //     break;
